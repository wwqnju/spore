# SPORE
SPORE: A sequential personalized spatial item recommender system

SPORE is a sequential personalized recommender system.
The details please refer to ICDE 2016 paper "SPORE: A sequential personalized spatial item recommender system".
The implementation is based on [standford nlp](http://stanfordnlp.github.io/CoreNLP/) library,
we include it in the lib directory.

For a quick start, you may run the main function in the model/SPORE.java

If the code is used in academic publication, please cite with following bibtex item:

```
#!html

@inproceedings{DBLP:conf/icde/WangYSCXZ16,
               author    = {Weiqing Wang and Hongzhi Yin and Shazia Wasim Sadiq and Ling Chen and Min Xie and Xiaofang Zhou},
               title     = {SPORE: {A} sequential personalized spatial item recommender system},
               booktitle = {ICDE},
               pages     = {954--965},
               year      = {2016}
}

```